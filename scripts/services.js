// get
// if()
var name = document.forms["tell-us"]["name"].value;
if(name==""){
    // document.getElementById("name-error").style.display="block";
}

var email = document.forms["tell-us"]["email"].value;
if(email==""){
    // document.getElementById("email-error").style.display="block";
}

var domain = document.forms["tell-us"]["domain"].value;
if(domain==""){
    // document.getElementById("domain-error").style.display="block";
}

var details = document.forms["tell-us"]["details"].value;
if(details==""){
    // document.getElementById("details-error").style.display="block";
}

document.getElementById('indirect-tax').onclick = function() {indirecttax()};

function indirecttax() {

  if(document.getElementById('indirect-tax-content').classList.contains('block-display')){
    document.getElementById('indirect-tax-content').classList.remove('block-display');
    document.getElementById('indirect-tax').classList.add('fa-plus');
    document.getElementById('indirect-tax').classList.remove('fa-minus');
  }else{
    console.log('in')
    document.getElementById('indirect-tax-content').classList.add('block-display');
    document.getElementById('indirect-tax').classList.remove('fa-plus');
    document.getElementById('indirect-tax').classList.add('fa-minus');
  }
}

document.getElementById('direct-tax').onclick = function() {directtax()};

function directtax() {
    
  if(document.getElementById('direct-tax-content').classList.contains('block-display')){
    document.getElementById('direct-tax-content').classList.remove('block-display');
    document.getElementById('direct-tax').classList.add('fa-plus');
    document.getElementById('direct-tax').classList.remove('fa-minus');
  }else{
    console.log('in')
    document.getElementById('direct-tax-content').classList.add('block-display');
    document.getElementById('direct-tax').classList.remove('fa-plus');
    document.getElementById('direct-tax').classList.add('fa-minus');
  }
}

document.getElementById('corporate-practice').onclick = function() {corporatepractice()};

function corporatepractice() {
    
  if(document.getElementById('corporate-practice-content').classList.contains('block-display')){
    document.getElementById('corporate-practice-content').classList.remove('block-display');
    document.getElementById('corporate-practice').classList.add('fa-plus');
    document.getElementById('corporate-practice').classList.remove('fa-minus');
  }else{
    console.log('in')
    document.getElementById('corporate-practice-content').classList.add('block-display');
    document.getElementById('corporate-practice').classList.remove('fa-plus');
    document.getElementById('corporate-practice').classList.add('fa-minus');
  }
}

document.getElementById('banking-practice').onclick = function() {bankingpractice()};

function bankingpractice() {
    
  if(document.getElementById('banking-practice-content').classList.contains('block-display')){
    document.getElementById('banking-practice-content').classList.remove('block-display');
    document.getElementById('banking-practice').classList.add('fa-plus');
    document.getElementById('banking-practice').classList.remove('fa-minus');
  }else{
    console.log('in')
    document.getElementById('banking-practice-content').classList.add('block-display');
    document.getElementById('banking-practice').classList.remove('fa-plus');
    document.getElementById('banking-practice').classList.add('fa-minus');
  }
}

document.getElementById('private-equity').onclick = function() {privateequity()};

function privateequity() {
    
  if(document.getElementById('private-equity-content').classList.contains('block-display')){
    document.getElementById('private-equity-content').classList.remove('block-display');
    document.getElementById('private-equity').classList.add('fa-plus');
    document.getElementById('private-equity').classList.remove('fa-minus');
  }else{
    console.log('in')
    document.getElementById('private-equity-content').classList.add('block-display');
    document.getElementById('private-equity').classList.remove('fa-plus');
    document.getElementById('private-equity').classList.add('fa-minus');
  }
}

document.getElementById('energy-practice').onclick = function() {energypractice()};

function energypractice() {
    
  if(document.getElementById('energy-practice-content').classList.contains('block-display')){
    document.getElementById('energy-practice-content').classList.remove('block-display');
    document.getElementById('energy-practice').classList.add('fa-plus');
    document.getElementById('energy-practice').classList.remove('fa-minus');
  }else{
    console.log('in')
    document.getElementById('energy-practice-content').classList.add('block-display');
    document.getElementById('energy-practice').classList.remove('fa-plus');
    document.getElementById('energy-practice').classList.add('fa-minus');
  }
}

document.getElementById('bankruptcy-practice').onclick = function() {bankruptcypractice()};

function bankruptcypractice() {
    
  if(document.getElementById('bankruptcy-practice-content').classList.contains('block-display')){
    document.getElementById('bankruptcy-practice-content').classList.remove('block-display');
    document.getElementById('bankruptcy-practice').classList.add('fa-plus');
    document.getElementById('bankruptcy-practice').classList.remove('fa-minus');
  }else{
    console.log('in')
    document.getElementById('bankruptcy-practice-content').classList.add('block-display');
    document.getElementById('bankruptcy-practice').classList.remove('fa-plus');
    document.getElementById('bankruptcy-practice').classList.add('fa-minus');
  }
}

document.getElementById('real-estate-practice').onclick = function() {realestate()};

function realestate() {
    
  if(document.getElementById('real-estate-practice-content').classList.contains('block-display')){
    document.getElementById('real-estate-practice-content').classList.remove('block-display');
    document.getElementById('real-estate-practice').classList.add('fa-plus');
    document.getElementById('real-estate-practice').classList.remove('fa-minus');
  }else{
    console.log('in')
    document.getElementById('real-estate-practice-content').classList.add('block-display');
    document.getElementById('real-estate-practice').classList.remove('fa-plus');
    document.getElementById('real-estate-practice').classList.add('fa-minus');
  }
}