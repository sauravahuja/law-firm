<?php

// $servername = "remotemysql.com";
// $username = "6YnxBrqv6N";
// $password = "YBExNF00cA";
// $dbname = "6YnxBrqv6N";
// $port = "3306";

$servername = "localhost";
$username = "sauravahuja";
$password = "sauravahuja";
$dbname = "lawfirm";
$port = "8111";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname, $port);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Law Firm | Service</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./css/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="./css/helper-classes.css">

    <link rel="stylesheet" href="./css/services.css">
    <link rel="stylesheet" href="./css/service-mobile.css">
    <link rel="stylesheet" href="./css/service-tab.css">

    <link rel="stylesheet" href="./css/navbar.css">
    <link rel="stylesheet" href="./css/navbar-mobile.css">
</head>

<body>
    <section id="navbar">
        <div class="logo">
            <img src="./images/logo/transparent.png" alt="">
            <h2 class="logo-name">Prestige Associates</h2>
        </div>
        <ul class="nav-list">
            <li><a href="./index.html" class="nav-links">Home</a></li>
            <li><a href="./about-us.html" class="nav-links">About Us</a></li>
            <li><a href="./services.php" class="nav-links">Services</a></li>
            <li><a href="./cost.php" class="nav-links">Costing</a></li>
            <li><a href="./appointment.php" class="nav-links">Appointment</a></li>
            <li><a href="./contact-us.php" class="nav-links">Contact Us</a></li>
        </ul>
    </section>
    <section id="mobile-nav">
        <div class="mobile-logo">
            <div class="logo">
                <img src="./images/logo/transparent.png" alt="">
                <h2 class="logo-name">Prestige Associates</h2>
            </div>
            <p><span class="fa fa-close close-btn" id="close"></span></p>
        </div>
        <div class="full-page-nav">
            <ul class="nav-list">
                <li><a href="./index.html" class="nav-links">Home</a></li>
                <li><a href="./about-us.html" class="nav-links">About Us</a></li>
                <li><a href="./services.php" class="nav-links">Services</a></li>
                <li><a href="./cost.php" class="nav-links">Costing</a></li>
                <li><a href="./appointment.php" class="nav-links">Appointment</a></li>
                <li><a href="./work-with-us.html" class="nav-links">Work With Us</a></li>
                <li><a href="./contact-us.php" class="nav-links">Contact Us</a></li>
            </ul>
        </div>
    </section>
    <section id="top-img">
        <img src="./images/service-image/publication.jpg" alt="" srcset="">
        <p><span class="fa fa-bars ham-bar" id="bars"></span></p>
    </section>
    <section id="practice-area-info">
        <div class="services-strap"></div>
        <div class="content-box-md">
            <div class="container">
                <div class="section-head text-center">
                    <h2 class="section-title">PRACTICE AREAS</h2>
                    <span class="s-dash"></span>
                </div>
                <div class="practice-area-content">
                    <div class="pracice-area">
                        <div class="area-name p-t-40">
                            <div class="area-head">
                                <p>Indirect Taxation</p>
                                <span class="fa fa-plus" id="indirect-tax"></span>
                            </div>
                            <div id="indirect-tax-content" class="content">
                                <p>The firm’s Indirect Tax team is well -equipped and dedicated to render advice and
                                    litigate across the entire spectrum of indirect tax laws. The team aims at
                                    suggesting practical and innovative solutions to its clients and has expertise in
                                    the following areas:
                                </p>
                                <p>
                                    Goods and Services Tax (‘GST’), Customs, Service Tax, Central Sales Tax/Value Added
                                    Tax
                                    (‘VAT’), Central Excise, etc. The firm has a dedicated team of experts who are
                                    engaged
                                    in providing, inter alia, the following services:
                                </p>
                                <ul class="p-t-10">
                                    <li> Litigation before all forums (Supreme Court, High Court, Tax Tribunals, and tax
                                        authorities)
                                    </li>
                                    <li>
                                        Advisory and Consultancy in areas like Banking, Finance, Leasing,
                                        Infrastructure, Real
                                        Estate, Hospitality, Software, Media and Entertainment, Advertising, Retail
                                        Trade,
                                        Management Consultancy, Education, Automobile, Auto-ancillaries, Airlines,
                                        Shipping
                                        Industries, etc.
                                    </li>
                                    <li>
                                        Managed Services in the area of Compliance and Regulatory matters on all India
                                        basis
                                        which includes within its ambit filing of returns, facilitation of audit,
                                        refunds under
                                        Foreign Trade Policy Schemes, etc.
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="area-name p-t-40">
                            <div class="area-head">
                                <p>Direct Taxation</p>
                                <span class="fa fa-plus" id="direct-tax"></span>
                            </div>
                            <div id="direct-tax-content" class="content">
                                <p>The Direct Tax team comprising lawyers and chartered accountants, many of whom have multiple qualifications, offers a comprehensive array of tax advisory and litigation services for businesses and organisations in various sectors.
                                </p>
                                <p>
                                    In the niche and sought after arena of Transfer Pricing, the firm has built capabilities to handle the entire gamut of services in relation thereto, right from the stage of compilation of data and preparation of transfer pricing reports to representation before the transfer pricing appellate authorities and courts. We understand the business, accounting and legal intricacies affecting taxation.
                                </p>
                                <ul class="p-t-10">
                                    <li> Litigation before all forums (Supreme Court, High Court, Tax Tribunals, and tax
                                        authorities)
                                    </li>
                                    <li>
                                        Advice and consultancy relating to both domestic and international tax issues
                                    </li>
                                    <li>
                                        Strategic tax planning
                                    </li>
                                    <li>
                                        Transfer Pricing – study, compliance and representation
                                    </li>
                                    <li>
                                        Compliance and Regulatory
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="area-name p-t-40">
                            <div class="area-head">
                                <p>Corporate Practice</p>
                                <span class="fa fa-plus" id="corporate-practice"></span>
                            </div>
                            <div id="corporate-practice-content" class="content">
                                <p>The Corporate team offers services across all stages of development experienced by a corporate entity, right from its inception to dissolution. At Prestige Associates Advocates, we follow a unique work ethic. For every transaction, be it Corporate, Real Estate, Regulatory, Labour and Employment, Finance Compliance or others, an efficient team of lawyers and professionals works closely to structure and execute the transaction, thereby providing the client a holistic solution.
                                </p>
                                <p>
                                    The team is well-acquainted with the international corporate environment and understands the requirements of both foreign clients and domestic clients engaged in cross-border transactions.

                                    The firm has a dedicated team of specialized professionals offering a range of legal and advisory services, covering all aspects related to foreign investments including the following:
                                </p>
                                <ul class="p-t-10">
                                    <li> Inbound and Outbound Investments
                                    </li>
                                    <li>
                                        Joint Ventures
                                    </li>
                                    <li>Financial and Technical Collaborations.
                                    </li>
                                    <li>
                                        Drafting of Distribution and Franchise Agreements
                                    </li>
                                    <li>External Commercial Borrowings (ECB) and Foreign Currency Convertible Bonds (FCCB)</li>
                                </ul>
                            </div>
                        </div>

                        <div class="area-name p-t-40">
                            <div class="area-head">
                                <p>Banking And Structured Finance</p>
                                <span class="fa fa-plus" id="banking-practice"></span>
                            </div>
                            <div id="banking-practice-content" class="content">
                                <p>
                                    We offer a full spectrum of services in the area of Banking and Finance by actively assisting corporate clients in drafting and reviewing various documents pertaining to all kinds of financial arrangements, including but not limited to equipment supply financing, acquisition financing, raising of non-convertible debentures, external commercial borrowings, term loan and working capital financing, factoring arrangements, securitization, acquiring pools of receivables by way of business transfer, advising on various fund based and fee based banking transactions, SARFAESI proceedings , banking laws and regulations, etc.
                                </p>
                                <p class="p-t-20">
                                    Our firm acts for myriad global and domestic banks, in addition to numerous institutional lenders, multi-lateral development and credit institutions, asset managers, funds, real estate investors, arrangers and corporate borrowers.
                                </p>
                            </div>
                        </div>

                        <div class="area-name p-t-40">
                            <div class="area-head">
                                <p>Private Equity</p>
                                <span class="fa fa-plus" id="private-equity"></span>
                            </div>
                            <div id="private-equity-content" class="content">
                                <p>Our Firm provides legal services to Private Equity and Venture Capital funds across the full range of their operations and activities. The areas in which we provide legal advice include:
                                </p>
                                <ul class="p-t-10">
                                    <li> advising on foreign investment issues, corporate and securities markets laws and issues applicable to investments
                                    </li>
                                    <li>
                                        documentation and overall transactional support, including working closely with regulators like the Reserve Bank of India (RBI), the Department of Industrial Policy and Promotion (DIPP) and the Securities &amp; Exchange Board of India (SEBI) and
                                    </li>
                                    <li>
                                        due diligence of prospective investee companies.
                                    </li>
                                    <li>
                                        advising on exit strategies and implementation thereof
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="area-name p-t-40">
                            <div class="area-head">
                                <p>Renewable Energy Practice</p>
                                <span class="fa fa-plus" id="energy-practice"></span>
                            </div>
                            <div id="energy-practice-content" class="content">
                                <p>The Renewable Energy Practice team has gained extensive experience in advising project owners, developers, contractors and suppliers on project development and related transactional issues across India. The predominant focus of our practice is the wind and solar energy sectors.
                                </p>
                                <p>
                                    The Firm advises not only on the acquisitions of existing and under development projects, but the entire cycle of green field project development. Our services cover:
                                </p>
                                <ul class="p-t-10">
                                    <li> Service Agreements
                                    </li>
                                    <li>
                                        Works Contracts
                                    </li>
                                    <li>
                                        Power Purchase Agreements and
                                    </li>
                                    <li>Wheeling and Banking Agreements</li>
                                    <li>Acquisition/ sale of energy assets, including</li>
                                    <li>Acquisition/ sale of equity stake in companies</li>
                                    <li>
                                        Acquisition of under development/ operational assets, by way of asset sale</li>
                                    <li>Structuring of investments</li>
                                    <li>Preparation/ review of definitive agreements related to such purchase/ sale</li>
                                    <li>Investment/ joint ventures in companies within the energy sector</li>
                                </ul>
                            </div>
                        </div>

                        <div class="area-name p-t-40">
                            <div class="area-head">
                                <p>Insolvency And Bankruptcy Practice</p>
                                <span class="fa fa-plus" id="bankruptcy-practice"></span>
                            </div>
                            <div id="bankruptcy-practice-content" class="content">
                                <p>
                                    The team has expertise advising on restructuring and all aspects of insolvency and bankruptcy including preparation of resolutions and notices; settling of outstanding liabilities; appearances before various authorities, courts, and tribunals.
                                </p>
                                <p>
                                    We advise secured and unsecured creditors on various aspects relating to claims against companies under Insolvency and Bankruptcy Code, 2016 including issues pertaining to enforcing security, representing clients in proceedings for directions and defending the corporate debtor.
                                </p>
                                <p>
                                    Our experience enables us to find practical and tailored solutions to the problems of corporate clients and smaller enterprises alike. We tailor our advice to the nature of the dispute and the appetite of the client. If a dispute arises, we help our clients navigate all types of commercial litigation in an effective manner. Our clients range from large multinational corporations to major financial institutions, as also high-profile individuals.
                                </p>
                            </div>
                        </div>

                        <div class="area-name p-t-40">
                            <div class="area-head">
                                <p>Real Estate And Construction</p>
                                <span class="fa fa-plus" id="real-estate-practice"></span>
                            </div>
                            <div id="real-estate-practice-content" class="content">
                                <p>
                                    Our firm advises on all aspects of corporate real estate laws, including cross-border real estate transactions, real estate investment banking, real estate transaction finance, joint ventures and project development, title due diligence and particularly, the areas of general real estate and project finance (including mortgage-based financing, leasing, etc.); structuring of real property holding companies (SPVs), etc.; commercial lease agreements and facility management agreements including litigation matters involving real estate and advising on Indian property and related laws pursuant to the Transfer of Property Act, Stamp and Registration laws, Land Revenue laws, and Tenancy laws in several States in India.
                                </p>
                                <p>
                                    Our real estate practice represents developers, investors, lenders, and business users in the acquisition, disposition, development, financing, construction, sale and leasing of real estate. We frequently assist in planning the structure of and negotiating complex real estate transactions, as well as handling the legal analysis and documentation.
                                </p>
                                <p>
                                    Our work spans all types of real estate from raw land to offices, apartments, retail, industrial, hotels, hospitals to complex mixed-use developments, and has involved projects throughout India.
                                </p>
                                <p>
                                    The team has developed a deep understanding of the real estate business and strives to provide our clients with sound legal and practical solutions. Our clients look to us to “get the deal done,” and we work proactively to resolve issues and close transactions swiftly.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="tell-us">
        <div class="content-box-md">
            <div class="container">
                <div class="section-head text-center">
                    <h2 class="section-title">LET US KNOW <br>ABOUT YOUR CASE</h2>
                    <span class="s-dash"></span>
                </div>
                <div class="tell-us-content p-t-60">
                    <form action="./services.php#tell-us" method="POST" name="tell-us">
                        <p class="error" id="error">Name Field Not Filled*</p>
                        <input type="text" name="name" id="name" placeholder="Enter Your Name">

                        <input type="text" name="email" id="email" placeholder="Enter Your Email Address">

                        <input type="text" name="domain" id="domain" placeholder="Enter Your Case Domain">

                        <textarea name="details" id="details" cols="30" rows="5" placeholder="Enter Your Case Details" class="m-t-10"></textarea>
                        <button type="submit" class="text-center">Submit My Case</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <?php
    if ($_POST) {
        // echo($_POST['name']);
        $name = $_POST['name'];
        $emailid = $_POST["email"];
        $domain = $_POST["domain"];
        $details = $_POST["details"];
        if ($name == "" || $emailid == "" || $domain == "" || $details == "") {
    ?>
            <script>
                alert("Fill All The Details");
            </script>
    <?php
        } else {
            $sql = "insert into case_details (name,email_id,domain,details) values ('" . $name . "','" . $emailid . "','" . $domain . "','" . $details . "')";
            $result = $conn->query($sql);
        }
    }
    ?>
    <footer id="footer">
        <div class="blue-strap"></div>
        <div class="grey-strap">
            <ul class="bottom-link">
                <li><a href="https://remotemysql.com/login.php" class="page-links">Database</a></li>
                <li><a href="./work-with-us.html" class="page-links">Work With Us</a></li>
                <li><a href="./disclaimer.html" class="page-links">Disclaimer</a></li>
                <li><a href="./contact-us.php" class="page-links">Contact Us</a></li>
            </ul>
            <p class="social-links">
                <span class="fa fa-linkedin"></span>
                <span class="fa fa-github"></span>
                <span class="fa fa-instagram"></span>
            </p>
        </div>
        <div class="copyrights text-center">
            <p>All Rights Reserved Copyright @2020</p>
        </div>
    </footer>

    <script src="./scripts/services.js"></script>
    <script src="./scripts/navbar.js"></script>
</body>

</html>