<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Law Firm | Contact Us</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./css/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="./css/helper-classes.css">

    <link rel="stylesheet" href="./css/contact-us.css">
    <link rel="stylesheet" href="./css/contact-us-mobile.css">
    <link rel="stylesheet" href="./css/contact-us-tab.css">

    <link rel="stylesheet" href="./css/navbar.css">
    <link rel="stylesheet" href="./css/navbar-mobile.css">
</head>

<body>
<section id="navbar">
        <div class="logo">
            <img src="./images/logo/transparent.png" alt="">
            <h2 class="logo-name">Prestige Associates</h2>
        </div>
        <ul class="nav-list">
            <li><a href="./index.html" class="nav-links">Home</a></li>
            <li><a href="./about-us.html" class="nav-links">About Us</a></li>
            <li><a href="./services.php" class="nav-links">Services</a></li>
            <li><a href="./cost.php" class="nav-links">Costing</a></li>
            <li><a href="./appointment.php" class="nav-links">Appointment</a></li>
            <li><a href="./contact-us.php" class="nav-links">Contact Us</a></li>
        </ul>
    </section>
    <section id="mobile-nav">
        <div class="mobile-logo">
            <div class="logo">
                <img src="./images/logo/transparent.png" alt="">
                <h2 class="logo-name">Prestige Associates</h2>
            </div>
            <p><span class="fa fa-close close-btn" id="close"></span></p>
        </div>
        <div class="full-page-nav">
            <ul class="nav-list">
                <li><a href="./index.html" class="nav-links">Home</a></li>
                <li><a href="./about-us.html" class="nav-links">About Us</a></li>
                <li><a href="./services.php" class="nav-links">Services</a></li>
                <li><a href="./cost.php" class="nav-links">Costing</a></li>
                <li><a href="./appointment.php" class="nav-links">Appointment</a></li>
                <li><a href="./work-with-us.html" class="nav-links">Work With Us</a></li>
                <li><a href="./contact-us.php" class="nav-links">Contact Us</a></li>
            </ul>
        </div>
    </section>
    <section id="top-img">
        <img src="./images/contact-us-image/notch/contactus.jpg" alt="">
        <p><span class="fa fa-bars ham-bar" id="bars"></span></p>
    </section>
    <section id="contact-section">
        <div class="contact-strap"></div>
        <div class="theme-bg"></div>
        <div class="container ">
            <div class="content-box-md">
                <div class="contact-content">
                    <div class="section-head text-center">
                        <h2 class="section-title">CONTACT US</h2>
                        <span class="s-dash"></span>
                    </div>
                    <div class="contact-us-content p-t-60 text-center">
                        <form action="./contact-us.php#contact-section" method="POST" name="tell-us">
                            <input type="text" name="name" id="name" placeholder="Enter Your Name">

                            <input type="text" name="email" id="email" placeholder="Enter Your Email Address">

                            <textarea name="queries" id="details" cols="30" rows="5" placeholder="Enter Your query" class="m-t-10"></textarea>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">Get In Touch</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="findus">
        <div class="section-head text-center p-t-50 p-b-50">
            <h2 class="section-title">FIND US HERE</h2>
            <span class="s-dash"></span>
        </div>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d60290.35844522863!2d72.80728533334398!3d19.18876317784313!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7b701648ae49f%3A0x5f26aa1cd1a28d4c!2sMalad%2C%20Reserve%20Bank%20of%20India%20Staff%20Quarters%2C%20Raheja%20Twp%2C%20Malad%20East%2C%20Mumbai%2C%20Maharashtra!5e0!3m2!1sen!2sin!4v1605336858651!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </section>
    <footer id="footer">
        <div class="blue-strap"></div>
        <div class="grey-strap">
            <ul class="bottom-link">
                <li><a href="https://remotemysql.com/login.php" class="page-links">Database</a></li>
                <li><a href="./work-with-us.html" class="page-links">Work With Us</a></li>
                <li><a href="./disclaimer.html" class="page-links">Disclaimer</a></li>
                <li><a href="./contact-us.php" class="page-links">Contact Us</a></li>
            </ul>
            <p class="social-links">
                <span class="fa fa-linkedin"></span>
                <span class="fa fa-github"></span>
                <span class="fa fa-instagram"></span>
            </p>
        </div>
        <div class="copyrights text-center">
            <p>All Rights Reserved Copyright @2020</p>
        </div>
    </footer>
    <script src="./scripts/navbar.js"></script>
</body>
<?php

// $servername = "remotemysql.com";
// $username = "6YnxBrqv6N";
// $password = "YBExNF00cA";
// $dbname = "6YnxBrqv6N";
// $port = "3306";

$servername = "localhost";
$username = "sauravahuja";
$password = "sauravahuja";
$dbname = "lawfirm";
$port = "8111";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname, $port);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
if ($_POST) {
    // echo($_POST['name']);
    $name = $_POST['name'];
    $emailid = $_POST["email"];
    $queries = $_POST["queries"];
    if ($name == "" || $emailid == "" || $queries == "") {
?>
        <script>
            alert("Fill All The Details");
        </script>
    <?php
    } else {
        $sql = "insert into contactus (name,email,queries) values ('" . $name . "','" . $emailid . "','" . $queries . "')";
        // die($sql);
        $result = $conn->query($sql);
    ?>
        <script>
            alert("Your Message Has Been Delivered");
        </script>
<?php
    }
}
?>

</html>