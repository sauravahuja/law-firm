<?php
$totalPrice = 0;
$index = 0;

// $servername = "remotemysql.com";
// $username = "6YnxBrqv6N";
// $password = "YBExNF00cA";
// $dbname = "6YnxBrqv6N";
// $port = "3306";

$servername = "localhost";
$username = "sauravahuja";
$password = "sauravahuja";
$dbname = "lawfirm";
$port = "8111";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname, $port);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Law Firm | Costing</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./css/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="./css/helper-classes.css">

    <link rel="stylesheet" href="./css/cost.css">
    <link rel="stylesheet" href="./css/cost-mobile.css">
    <link rel="stylesheet" href="./css/cost-tab.css">

    <link rel="stylesheet" href="./css/navbar.css">
    <link rel="stylesheet" href="./css/navbar-mobile.css">
</head>

<body>
<section id="navbar">
        <div class="logo">
            <img src="./images/logo/transparent.png" alt="">
            <h2 class="logo-name">Prestige Associates</h2>
        </div>
        <ul class="nav-list">
            <li><a href="./index.html" class="nav-links">Home</a></li>
            <li><a href="./about-us.html" class="nav-links">About Us</a></li>
            <li><a href="./services.php" class="nav-links">Services</a></li>
            <li><a href="./cost.php" class="nav-links">Costing</a></li>
            <li><a href="./appointment.php" class="nav-links">Appointment</a></li>
            <li><a href="./contact-us.php" class="nav-links">Contact Us</a></li>
        </ul>
    </section>
    <section id="mobile-nav">
        <div class="mobile-logo">
            <div class="logo">
                <img src="./images/logo/transparent.png" alt="">
                <h2 class="logo-name">Prestige Associates</h2>
            </div>
            <p><span class="fa fa-close close-btn" id="close"></span></p>
        </div>
        <div class="full-page-nav">
            <ul class="nav-list">
                <li><a href="./index.html" class="nav-links">Home</a></li>
                <li><a href="./about-us.html" class="nav-links">About Us</a></li>
                <li><a href="./services.php" class="nav-links">Services</a></li>
                <li><a href="./cost.php" class="nav-links">Costing</a></li>
                <li><a href="./appointment.php" class="nav-links">Appointment</a></li>
                <li><a href="./work-with-us.html" class="nav-links">Work With Us</a></li>
                <li><a href="./contact-us.php" class="nav-links">Contact Us</a></li>
            </ul>
        </div>
    </section>
    <section id="top-img">
        <img src="./images/service-image/publication.jpg" alt="" srcset="">
        <p><span class="fa fa-bars ham-bar" id="bars"></span></p>
    </section>
    <section id="practice-area-info">
        <div class="blue-strap"></div>
        <div class="content-box-md">
            <div class="container">
                <div class="section-head text-center">
                    <h2 class="section-title">Costing For Services</h2>
                    <span class="s-dash"></span>
                </div>
                <p class="text-center p-t-20 para-content">To find out the costing for the paticular service you need,<br> Please select the services you want and click on calculate cost button !</p>
                <form action="./cost.php#cost-values" method="POST" class="m-t-40">
                    <label><input type="checkbox" id="vehicle1" name="service1" value="Document Verification">&nbsp;&nbsp;&nbsp;Document Verification</label>
                    <label><input type="checkbox" id="vehicle2" name="service2" value="Case Study">&nbsp;&nbsp;&nbsp;Case Study</label>
                    <label><input type="checkbox" id="vehicle3" name="service3" value="Real Estate and Construction">&nbsp;&nbsp;&nbsp;Real Estate and Construction</label>
                    <label><input type="checkbox" id="vehicle3" name="service4" value="Non Profit Organization Registration">&nbsp;&nbsp;&nbsp;Non Profit Organization Registration</label>
                    <label><input type="checkbox" id="vehicle3" name="service5" value="Taxation Clearance">&nbsp;&nbsp;&nbsp;Taxation Clearance</label>
                    <label><input type="checkbox" id="vehicle3" name="service6" value="Business Registration">&nbsp;&nbsp;&nbsp;Business Registration</label>
                    <label><input type="checkbox" id="vehicle3" name="service7" value="Private Equity">&nbsp;&nbsp;&nbsp;Private Equity</label>
                    <input type="submit" value="Calculate Total Cost" class="btn btn-primary m-t-20">
                </form>
            </div>
        </div>
    </section>
    
    <section id="cost-values" class="m-b-40">
        <div class="container">
        <table>
                <tr>
                    <th style="width:5%">Index</th>
                    <th>Service</th>
                    <th>Cost</th>
                </tr>
                <?php
                if ($_POST) {
                    foreach ($_POST as $key => $value) {
                        $sql = 'select price from service_details where service="' . $value . '"';
                        $result = $conn->query($sql);
                        $index++;
                        if ($result) {
                            // output data of each row
                            while ($row = $result->fetch_assoc()) {
                ?>
                                <tr>

                                    <td><?= $index ?></td>
                                    <td><?= $value ?></td>
                                    <td>Rs. <?= $row["price"] ?>/-</td>

                                </tr>
                <?php
                                $totalPrice = $totalPrice + $row["price"];
                            }
                        }
                    }
                }
                ?>
                <tr>
                    <td></td>
                    <td><strong>Total Cost Of The Services Selected</strong></td>
                    <td><strong>Rs. <?= $totalPrice ?>/-</strong></td>
                </tr>
            </table>
        </div>
    </section>
    <footer id="footer">
        <div class="blue-strap"></div>
        <div class="grey-strap">
            <ul class="bottom-link">
                <li><a href="https://remotemysql.com/login.php" class="page-links">Database</a></li>
                <li><a href="./work-with-us.html" class="page-links">Work With Us</a></li>
                <li><a href="./disclaimer.html" class="page-links">Disclaimer</a></li>
                <li><a href="./contact-us.php" class="page-links">Contact Us</a></li>
            </ul>
            <p class="social-links">
                <span class="fa fa-linkedin"></span>
                <span class="fa fa-github"></span>
                <span class="fa fa-instagram"></span>
            </p>
        </div>
        <div class="copyrights text-center">
            <p>All Rights Reserved Copyright @2020</p>
        </div>
    </footer>

    <script src="./scripts/navbar.js"></script>
</body>

</html>