<?php
$servername = "localhost";
$username = "sauravahuja";
$password = "sauravahuja";
$dbname = "test";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}else{
    echo($servername."<br>".$username."<br>".$password."<br>".$dbname);
    echo("<br>");
    echo("<h1>Connection Has Been Successfully Established</h1>");
}

// $sql = "SELECT topic FROM books";
// $result = $conn->query($sql);
// // var_dump($result);
// while($row = $result->fetch_assoc()) {
//     echo "id: " . $row["topic"]."<br>";
//   }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Testing</title>

    <!-- SLICK CSS -->
    <link rel="stylesheet" href="./css/plugins/slick-1.8.1/slick/slick.css">
    <link rel="stylesheet" href="./css/plugins/slick-1.8.1/slick/slick-theme.css">

    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="./css/plugins/bootstrap-4.5.2-dist/css/bootstrap.min.css">
    <style>
        body{
            background: #ccc
            ;
        }
        .content-box {
            background: #FC3;
            color: #333;
            padding: 20px;
            text-align: center;
        }
        .row div{
            background: #333;
            color: #fff;
            padding: 20px;
        }
    </style>
</head>

<body>
    <!-- <div class="your-class">
        <div class="content-box">your content 1</div>
        <div class="content-box">your content 2</div>
        <div class="content-box">your content 3</div>
    </div>
    <div class="container">
        <div class="card-deck mt-md-3">
            <div class="card">
                <img src="../w3-Templates/w3-Band/images/bandmember.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to
                        additional content. This content is a little bit longer.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>

            <div class="card">
                <img src="../w3-Templates/w3-Band/images/bandmember.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to
                        additional content. This content is a little bit longer.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>

            <div class="card">
                <img src="../w3-Templates/w3-Band/images/bandmember.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to
                        additional content. This content is a little bit longer.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <!-- Stack the columns on mobile by making one full-width and the other half-width -->
        <!-- <div class="row">
          <div class="col-md-8">.col-md-8</div>
          <div class="col-6 col-md-4">.col-6 .col-md-4</div>
        </div> -->
      
        <!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
        <!-- <div class="row">
          <div class="col-6 col-md-4">.col-6 .col-md-4</div>
          <div class="col-6 col-md-4">.col-6 .col-md-4</div>
          <div class="col-6 col-md-4">.col-6 .col-md-4</div>
        </div> -->
      
        <!-- Columns are always 50% wide, on mobile and desktop -->
        <!-- <div class="row">
          <div class="col-6">.col-6</div>
          <div class="col-6">.col-6</div>
        </div>
      </div> -->
    <!-- JQUERY SCRIPT -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

    <!-- SLICK SCRIPT -->
    <script src="assets/css/plugins/slick-1.8.1/slick/slick.min.js"></script>

    <!-- BOOTSTRAP SCRIPT -->
    <script src="./css/plugins/bootstrap-4.5.2-dist/js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function () {
            $('.your-class').slick({
                autoplay: true,
                adaptiveHeight: true,
                autoplaySpeed: 3000,

            });
        });
    </script>
</body>

</html>