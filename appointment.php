<?php
$time = "none";
// $servername = "remotemysql.com";
// $username = "6YnxBrqv6N";
// $password = "YBExNF00cA";
// $dbname = "6YnxBrqv6N";
// $port = "3306";

$servername = "localhost";
$username = "sauravahuja";
$password = "sauravahuja";
$dbname = "lawfirm";
$port = "8111";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname, $port);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Law Firm | Service</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./css/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="./css/helper-classes.css">

    <link rel="stylesheet" href="./css/services.css">
    <link rel="stylesheet" href="./css/service-mobile.css">
    <link rel="stylesheet" href="./css/service-tab.css">
    <link rel="stylesheet" href="./css/appointment.css">

    <link rel="stylesheet" href="./css/navbar.css">
    <link rel="stylesheet" href="./css/navbar-mobile.css">
</head>

<body>
<section id="navbar">
        <div class="logo">
            <img src="./images/logo/transparent.png" alt="">
            <h2 class="logo-name">Prestige Associates</h2>
        </div>
        <ul class="nav-list">
            <li><a href="./index.html" class="nav-links">Home</a></li>
            <li><a href="./about-us.html" class="nav-links">About Us</a></li>
            <li><a href="./services.php" class="nav-links">Services</a></li>
            <li><a href="./cost.php" class="nav-links">Costing</a></li>
            <li><a href="./appointment.php" class="nav-links">Appointment</a></li>
            <li><a href="./contact-us.php" class="nav-links">Contact Us</a></li>
        </ul>
    </section>
    <section id="mobile-nav">
        <div class="mobile-logo">
            <div class="logo">
                <img src="./images/logo/transparent.png" alt="">
                <h2 class="logo-name">Prestige Associates</h2>
            </div>
            <p><span class="fa fa-close close-btn" id="close"></span></p>
        </div>
        <div class="full-page-nav">
            <ul class="nav-list">
                <li><a href="./index.html" class="nav-links">Home</a></li>
                <li><a href="./about-us.html" class="nav-links">About Us</a></li>
                <li><a href="./services.php" class="nav-links">Services</a></li>
                <li><a href="./cost.php" class="nav-links">Costing</a></li>
                <li><a href="./appointment.php" class="nav-links">Appointment</a></li>
                <li><a href="./work-with-us.html" class="nav-links">Work With Us</a></li>
                <li><a href="./contact-us.php" class="nav-links">Contact Us</a></li>
            </ul>
        </div>
    </section>
    <section id="top-img">
        <img src="./images/service-image/publication.jpg" alt="" srcset="">
        <p><span class="fa fa-bars ham-bar" id="bars"></span></p>
    </section>
    <section id="appointment">
        <div class="content-box-md">
            <div class="container">
                <div class="section-head text-center">
                    <h2 class="section-title">BOOK AN APPOINTMENT</h2>
                    <span class="s-dash"></span>
                </div>
                <div class="appointment-content p-t-60">
                    <form action="./appointment.php#appointment" method="POST" class="m-t-40">
                        <p class="date-class m-b-20"><?= "" . date("d/m/Y") . ",&nbsp;" . date("l")."<br>" ?></p>
                        <label id="ts-label-1"><input type="radio" id="1" name="time" value="10am - 11am">&nbsp;&nbsp;&nbsp;10am - 11am</label>
                        <label id="ts-label-2"><input type="radio" id="2" name="time" value="11am - 12pm">&nbsp;&nbsp;&nbsp;11am - 12pm</label>
                        <label id="ts-label-3"><input type="radio" id="3" name="time" value="1pm - 2pm">&nbsp;&nbsp;&nbsp;1pm - 2pm</label>
                        <label id="ts-label-4"><input type="radio" id="4" name="time" value="2pm - 3pm">&nbsp;&nbsp;&nbsp;2pm - 3pm</label>
                        <input type="submit" value="Book" class="btn btn-primary m-t-20">
                    </form>
                </div>
            </div>
        </div>
    </section>
    <?php

    if (true) {
        $sql = "select id,appointment_time,booked from appointment;";
        $result = $conn->query($sql);
        if ($result) {
            while ($row = $result->fetch_assoc()) {
                if ($row['booked'] == 1) {
    ?>
                    <script>
                        for (i = 0; i < 4; i++) {
                            const a = document.getElementById("<?= $row['id'] ?>")
                            // console.log(a);
                            a.disabled = "true";
                            const b = document.getElementById("ts-label-<?= $row['id'] ?>");
                            console.log(b);
                            b.style.background = "#EEE";
                            b.style.color = "#999";
                        }
                    </script>
        <?php
                }
            }
        }
    }
    if ($_POST) {
        $time = $_POST['time'];
        $sql = 'update appointment set booked = 1 where appointment_time ="' . $time . '";';
        $result = $conn->query($sql);
        ?>
        <script>
            alert('Appointment Has Been Booked');
        </script>
        <?php
        $sql = "select id,appointment_time,booked from appointment;";
        $result = $conn->query($sql);
        if ($result) {
            while ($row = $result->fetch_assoc()) {
                if ($row['booked'] == 1) {
        ?>
                    <script>
                        for (i = 0; i < 4; i++) {
                            const a = document.getElementById("<?= $row['id'] ?>")
                            // console.log(a);
                            a.disabled = "false";
                            const b = document.getElementById("ts-label-<?= $row['id'] ?>");
                            console.log(b);
                            b.style.background = "#eee";
                            b.style.color = "#999";
                        }
                    </script>
    <?php
                }
            }
        }
    }
    ?>
    <footer id="footer">
        <div class="blue-strap"></div>
        <div class="grey-strap">
            <ul class="bottom-link">
                <li><a href="https://remotemysql.com/login.php" class="page-links">Database</a></li>
                <li><a href="./work-with-us.html" class="page-links">Work With Us</a></li>
                <li><a href="./disclaimer.html" class="page-links">Disclaimer</a></li>
                <li><a href="./contact-us.php" class="page-links">Contact Us</a></li>
            </ul>
            <p class="social-links">
                <span class="fa fa-linkedin"></span>
                <span class="fa fa-github"></span>
                <span class="fa fa-instagram"></span>
            </p>
        </div>
        <div class="copyrights text-center">
            <p>All Rights Reserved Copyright @2020</p>
        </div>
    </footer>
    <script src="./scripts/navbar.js"></script>
</body>

</html>